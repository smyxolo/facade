package Zadanie1;

public class AdvancedCoffeeMachine {

    private CoffeeBeverage coffeeBeverage;

    public AdvancedCoffeeMachine() {
        this.coffeeBeverage = new CoffeeBeverage();
    }

    public void addIngredient(CoffeeIngredient coffeeIngredient){
        coffeeBeverage.ingredientList.add(coffeeIngredient);
    }

    public CoffeeBeverage makeCoffeeBeverage(){
        return coffeeBeverage;
    }
}
