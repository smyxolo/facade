package Zadanie1.Production;

import Zadanie1.*;
import Zadanie1.Frother.FoamedMilkIngredient;
import Zadanie1.Frother.MilkFrother;
import Zadanie1.Frother.MilkFrotherException;
import Zadanie1.Grinder.CoffeeGrinderException;
import Zadanie1.Grinder.CoffeePowderIngredient;
import Zadanie1.Grinder.Grinder;

public class MainBarista {
    public static void main(String[] args) {
        AdvancedCoffeeMachine coffeeMachine = new AdvancedCoffeeMachine();
        Grinder grinder = new Grinder();
        MilkFrother milkFrother = new MilkFrother();

        //Espresso
        CoffeeBeverage espresso;
        CoffeeBean coffeeBean = new CoffeeBean();
        WaterIngredient waterIngredient = new WaterIngredient(30);

        CoffeePowderIngredient coffeePowderIngredient = null;
        while (coffeePowderIngredient == null) {
            try {
                coffeePowderIngredient = grinder.grind(coffeeBean);
            } catch (CoffeeGrinderException e) {
                grinder.empty();
            }
        }


        coffeeMachine.addIngredient(coffeePowderIngredient);
        coffeeMachine.addIngredient(waterIngredient);

        espresso = coffeeMachine.makeCoffeeBeverage();
        System.out.println(espresso);

        // 2x Latte
        for (int i = 0; i < 2; i++) {
            CoffeeBeverage latte = new CoffeeBeverage();
            CoffeeBean coffeeBean2 = new CoffeeBean();
            WaterIngredient waterIngredient2 = new WaterIngredient(100);
            MilkIngredient milkIngredient2 = new MilkIngredient();
            SugarIngredient sugarIngredient2 = new SugarIngredient();
            FoamedMilkIngredient foamedMilkIngredient = null;
            CoffeePowderIngredient coffeePowderIngredient2 = null;

            while (coffeePowderIngredient2 == null) {
                try {
                    coffeePowderIngredient2 = grinder.grind(coffeeBean2);
                } catch (CoffeeGrinderException e) {
                    grinder.empty();
                }
            }

            latte.getIngredientList().add(coffeePowderIngredient2);
            latte.getIngredientList().add(waterIngredient2);

            while (foamedMilkIngredient == null) {
                try {
                    foamedMilkIngredient = milkFrother.froth(milkIngredient2);
                } catch (MilkFrotherException e) {
                    System.out.println("MilkFrother failed.");
                }
            }

            latte.getIngredientList().add(foamedMilkIngredient);
            latte.getIngredientList().add(sugarIngredient2);

            System.out.println(latte);
        }
        CoffeeBeverage americano = new CoffeeBeverage();

        CoffeeBean coffeeBean3 = new CoffeeBean();
        CoffeePowderIngredient coffeePowderIngredient3 = null;
        WaterIngredient waterIngredient3 = new WaterIngredient(100);
        SugarIngredient sugarIngredient3 = new SugarIngredient();

        while (coffeePowderIngredient3 == null){
            try {
                coffeePowderIngredient3 = grinder.grind(coffeeBean3);
            }catch (CoffeeGrinderException e){
                grinder.empty();
            }
        }

        americano.getIngredientList().add(coffeePowderIngredient3);
        americano.getIngredientList().add(waterIngredient3);
        americano.getIngredientList().add(sugarIngredient3);

        System.out.println(americano);

    }

}
