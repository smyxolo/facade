package Zadanie1.Production;

public class Main {
    public static void main(String[] args) {
        SimpleCoffeeMachineFACADE scm = new SimpleCoffeeMachineFACADE();
        System.out.println(scm.makeCappucino());
        System.out.println(scm.makeEspresso());
        System.out.println(scm.makeLatte());
        System.out.println(scm.makeAmericano());
        System.out.println(scm.makeCappucino());
    }
}
