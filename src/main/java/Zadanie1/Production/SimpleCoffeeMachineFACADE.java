package Zadanie1.Production;

import Zadanie1.*;
import Zadanie1.Frother.FoamedMilkIngredient;
import Zadanie1.Frother.MilkFrother;
import Zadanie1.Frother.MilkFrotherException;
import Zadanie1.Grinder.CoffeeGrinderException;
import Zadanie1.Grinder.CoffeePowderIngredient;
import Zadanie1.Grinder.Grinder;

public class SimpleCoffeeMachineFACADE {
    MilkFrother mf = new MilkFrother();
    Grinder cg = new Grinder();
    CoffeeBeverage coffee = new CoffeeBeverage();

    public CoffeeBeverage makeCappucino(){
        CoffeeBean bean = new CoffeeBean();
        CoffeePowderIngredient coffeePowder = null;
        WaterIngredient water = new WaterIngredient(30);
        MilkIngredient milk = new MilkIngredient();
        MilkIngredient milkToFoam = new MilkIngredient();
        FoamedMilkIngredient foamedMilk = null;
        SugarIngredient sugar = new SugarIngredient();


        while (coffeePowder == null){
            try {
                coffeePowder = cg.grind(bean);
            } catch (CoffeeGrinderException e) {
                cg.empty();
            }
        }


        coffee.getIngredientList().add(coffeePowder);
        coffee.getIngredientList().add(water);
        coffee.getIngredientList().add(milk);

        while (foamedMilk == null){
            try {
                foamedMilk = mf.froth(milkToFoam);
            } catch (MilkFrotherException e) {
                System.out.println("Fighting with broken frother...");
            }
        }

        coffee.getIngredientList().add(foamedMilk);
        coffee.getIngredientList().add(sugar);

        CoffeeBeverage finishedCoffee = coffee;
        coffee = new CoffeeBeverage();
        return finishedCoffee;

    }

    public CoffeeBeverage makeLatte(){
        CoffeeBean bean = new CoffeeBean();
        CoffeePowderIngredient coffeePowder = null;
        WaterIngredient water = new WaterIngredient(100);
        MilkIngredient milk1 = new MilkIngredient();
        FoamedMilkIngredient foamedMilk = null;
        SugarIngredient sugar = new SugarIngredient();


        while (coffeePowder == null){
            try {
                coffeePowder = cg.grind(bean);
            } catch (CoffeeGrinderException e) {
                cg.empty();
            }
        }

        coffee.getIngredientList().add(coffeePowder);
        coffee.getIngredientList().add(water);

        while (foamedMilk == null){
            try {
                foamedMilk = mf.froth(milk1);
            } catch (MilkFrotherException e) {
                System.out.println("Fighting with broken frother...");
            }
        }

        coffee.getIngredientList().add(foamedMilk);
        coffee.getIngredientList().add(sugar);

        CoffeeBeverage finishedCoffee = coffee;
        coffee = new CoffeeBeverage();
        return finishedCoffee;

    }

    public CoffeeBeverage makeAmericano(){
        CoffeeBean bean = new CoffeeBean();
        CoffeePowderIngredient coffeePowder = null;
        WaterIngredient water = new WaterIngredient(100);
        SugarIngredient sugar = new SugarIngredient();


        while (coffeePowder == null){
            try {
                coffeePowder = cg.grind(bean);
            } catch (CoffeeGrinderException e) {
                cg.empty();
            }
        }

        coffee.getIngredientList().add(coffeePowder);
        coffee.getIngredientList().add(water);
        coffee.getIngredientList().add(sugar);

        CoffeeBeverage finishedCoffee = coffee;
        coffee = new CoffeeBeverage();
        return finishedCoffee;

    }

    public CoffeeBeverage makeEspresso(){
        CoffeeBean bean = new CoffeeBean();
        CoffeePowderIngredient coffeePowder = null;
        WaterIngredient water = new WaterIngredient(30);


        while (coffeePowder == null){
            try {
                coffeePowder = cg.grind(bean);
            } catch (CoffeeGrinderException e) {
                cg.empty();
            }
        }

        coffee.getIngredientList().add(coffeePowder);
        coffee.getIngredientList().add(water);

        CoffeeBeverage finishedCoffee = coffee;
        coffee = new CoffeeBeverage();
        return finishedCoffee;

    }




}
