package Zadanie1;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;


@Getter
public class CoffeeBeverage {
    List<CoffeeIngredient> ingredientList = new ArrayList<>();

    @Override
    public String toString() {
        return "Coffee: " + ingredientList.toString();
    }
}
