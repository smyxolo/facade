package Zadanie1;

public class WaterIngredient extends CoffeeIngredient {
    private int volume;

    public WaterIngredient(int volume) {
        this.volume = volume;
    }

    @Override
    public String toString() {
        return "Water{" +
                "volume=" + volume +
                '}';
    }
}
