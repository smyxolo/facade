package Zadanie1;

public class SugarIngredient extends CoffeeIngredient {
    @Override
    public String toString() {
        return "Sugar";
    }
}
