package Zadanie1.Frother;

import Zadanie1.CoffeeIngredient;

public class FoamedMilkIngredient extends CoffeeIngredient {

    FoamedMilkIngredient() {
    }

    @Override
    public String toString() {
        return "Foamed Milk";
    }
}
