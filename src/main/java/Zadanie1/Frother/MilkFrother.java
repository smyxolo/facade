package Zadanie1.Frother;

import Zadanie1.MilkIngredient;

public class MilkFrother {

    public FoamedMilkIngredient froth(MilkIngredient milkIngredient) throws MilkFrotherException {
        if(Math.random() > 0.8) throw new MilkFrotherException("Milk frother failed.");
        else return new FoamedMilkIngredient();
    }
}
