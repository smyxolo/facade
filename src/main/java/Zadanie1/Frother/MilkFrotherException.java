package Zadanie1.Frother;

public class MilkFrotherException extends Exception {
    public MilkFrotherException(String message) {
        super(message);
    }
}
