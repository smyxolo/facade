package Zadanie1.Grinder;

public class CoffeeGrinderException extends Exception {
    CoffeeGrinderException(){
        super("Waste container full. Please empty container to continue.");
    }
}
