package Zadanie1.Grinder;

import Zadanie1.CoffeeBean;

public class Grinder {
    private int wasteContainerCount = 0;

    public void empty(){
        System.out.println("Emptying container...");
        wasteContainerCount = 0;
    }

    public CoffeePowderIngredient grind(CoffeeBean coffeeBean) throws CoffeeGrinderException {
        if(wasteContainerCount >= 3) throw new CoffeeGrinderException();
        else {
            wasteContainerCount++;
            if(wasteContainerCount == 3) System.out.println("Waste container full");
            return new CoffeePowderIngredient();
        }

    }
}
