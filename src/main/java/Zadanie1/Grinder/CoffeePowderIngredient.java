package Zadanie1.Grinder;

import Zadanie1.CoffeeIngredient;

public class CoffeePowderIngredient extends CoffeeIngredient {
    CoffeePowderIngredient() {
    }

    @Override
    public String toString() {
        return "Coffee Powder";
    }
}
