Część 1 – Przygotowanie stanowiska pracy baristy
1. 2.
3. 4.
5. 6.
7.
8. 9.
10.
Stwórzmy klasę CoffieBean
Stwórzmy klasę abstrakcyjną CoffeeIngredient oraz klasy pochodne CoffeePowderIngredient, MilkIngredient, FoamedMilkIngredient, WaterIngredient oraz SugarIngredient. Klasa WaterIngredient będzie przyjmowała dodatakowy parametr do konstruktora zawierający informacje o ilości wody (w mililitrach).
Stwórzmy klasę CoffeeBeverage która będzie zawierała w sobie pole typu List<CoffeeIngredient> oraz zaimplementowaną metodę toString w taki sposób waby wyświetliła nam swój skład.
Stwórz pakiet grinder i umieść w nim klasę CoffePowderIngredient oraz dodaj do niej konstruktor o zasięgu package-private (bez public, private ani protected).
Stwórz klasę CoffieGrinderException która dziedziczy po Exception
Stwórz klasę CoffeGrinder w pakiecie grinder z polem wasteContainerCount typu int oraz metodami:
a. empty() - ustawiającą wartość pola wasteContainerCount na 0
b. grind() - przyjmującą jako parametr obiekt typu CoffieBean oraz zwracającą obiekt typu
CoffeePowderIngredient. Metoda sprawdza czy wasteContainerCount jest większy niż trzy. W przypadku gdy jest niech rzuci wyjątek CoffieGrinderException ze stosownym komunikatem. Jeśli wasteContainerCount jest mniejszy niż trzy inkrementujemy jego wartość i wyświetlamy w konsoli informacje o zapełnieniu „śmietniczka”.
Stwórz pakiet frother i umieść w nim klasę FoamedMilkIngredient oraz dodaj do niej konstruktor o zasięgu package-private (bez public, private ani protected).
Stwórzmy klasę MilkFrotherException która dziedziczy po Exception
Stwórzmy sobie klasę MilkFrother w pakiecie frother która będzie miała metodę froth() która przyjmie jako parametr obiekt typu MilkIngredient oraz zwróci obiekt typu FoamedMilkIngredient. Dodatkowo metoda sprawdzi czy wartość zwrócona przez Math.random() jest większa niż 0.8. Jeśli tak rzuci wyjątek MilkFrotherException.
Stwórz klasę AdvancedCoffeeMachine która będzie miała dwie metody:
a. addIngredient – metoda przyjmę jako parametr obiekt typu CoffeeIngredient i doda go do tworzonej
kawy, metoda nie zwraca żadnej wartości.
b. makeCoffeeBeverage – metoda zwróci nam obiekt typu CoffeeBeverage zawierający wszystkie
wcześniej dodane składniki. Metoda jednocześnie przygotuje nasz obiekt do przygotowania nowej kawy.
Część 2 – Praca baristy
1. Nasz barista potrafi robić następujące rodzaje kawy:
a. Espresso – składa się z kawy, 30ml wody
b. Latte – składa się z kawy, 100ml wody, spienionego mleka oraz opcjonalnie cukru.
c. Cappuccino – składa się z kawy, 30ml wody, mleka, spienionego mleka oraz opcjonalnie cukru
d. Amerykanka – składa się z kawy, 100ml wody i opcjonalnie cukru
2. Napisz program, który korzystając z klas stworzonych w części pierwszej zrobi nam kilka wybranych przez Ciebie kaw pod rząd. Na przykład 1 latte, 2 espresso, 1 amerykana i 2 cappuccino. Celowo wykonajmy to ćwiczenie powtarzając po sobie ten sam kod (co nie jest dobrą praktyką).
Część 3 – Automatyczny ekspres do kawy – Fasada
1. Stwórz klasę SimpleCoffeeMachine która będzie miała cztery metody zwracające CoffeBeverage: a. makeEspresso
b. makeLatte
c. makeAmericana d. makeCappuccino