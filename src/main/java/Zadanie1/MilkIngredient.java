package Zadanie1;

public class MilkIngredient extends CoffeeIngredient {

    @Override
    public String toString() {
        return "Milk";
    }
}
